import * as React from 'react';
import axios from 'axios'
import {Card, Table, Form, Button, Container} from 'react-bootstrap'

interface Props {
  name: string;
}

class App extends React.Component<Props, {urls: [], searchTerm: string, pageNumber: number, groups: any, totalImages: number, errorStatus?: boolean, searchStatus?: boolean}> {
  renderSearchResults = () => {
    if(this.state.errorStatus) {
      return <h3>Oops, seems like there was an issue finding your gifs. How embarressing....</h3>
    } 

    if(this.state.searchStatus && this.state.urls.length === 0) {
      return <h4>Looks like your search results are empty :(</h4>
    } 

    if(!this.state.searchStatus) {
      return null
    }

    if(this.state.searchStatus && this.state.urls.length > 0) {
      return (
        <Container>
          <Table className={"Table"}><thead><tr></tr></thead>
              <tbody>
                { this.state.groups.map((group: any[]) =>  (
                    <tr>
                      <td className={"ImageCell"}><img width={'150px'} height={"80px"} src={group[0]}></img></td>
                      <td className={"ImageCell"}><img width={'150px'} height={"80px"} src={group[1]}></img></td>
                      <td className={"ImageCell"}><img width={'150px'} height={"80px"} src={group[2]}></img></td>
                      <td className={"ImageCell"}><img width={'150px'} height={"80px"} src={group[3]}></img></td>

                    </tr>
                  ))
                }
              </tbody>
          </Table> 
          <Button disabled={this.state.pageNumber === 0} className={"Button"} style={{width: '125px'}} onClick={() => {
            console.log(this.state.pageNumber)
            this.setState({...this.state, pageNumber: this.state.pageNumber - 1})
            this.findGifs(this.state.pageNumber - 1)}}>Previous Page
          </Button>
          <Button disabled={this.state.pageNumber + 1 === Math.ceil(this.state.totalImages / 24)} className={"Button"} style={{width: '125px',marginRight: '10px'}} onClick={() => {
            this.setState({...this.state, pageNumber: this.state.pageNumber + 1})
            console.log(this.state.pageNumber)
            this.findGifs(this.state.pageNumber + 1)}}>Next Page
          </Button>
            Page: {this.state.pageNumber + 1} of {Math.ceil(this.state.totalImages / 24)}
        </Container>
      )
    }
    return null
  }
  findGifs = async (offset: number) => {
    const data = await axios.get(`https://api.giphy.com/v1/gifs/search?api_key=bXl4xKwDXVwh55xQDhuV8Fb2UieAZ2VK&q=${this.state.searchTerm}&limit=24&offset=${offset * 24}&rating=g&lang=en`, { responseType: 'json' })
    const urls = data.data.data.map((imageHolder: any) => imageHolder.images.fixed_height_small.url)
    
    let groups: any[][] = []
    if(data.status === 200) {
      urls.forEach((url: string, index: number) => {
        if(!groups[Math.floor(index / 4)]) {
          groups[Math.floor(index / 4)] = [url];
        } else {
          groups[Math.floor(index / 4)].push(url);
        }
      } )
      this.setState({urls: urls, groups: groups, totalImages: data.data.pagination.total_count, searchStatus: true});
    } else {
      this.setState({...this.state, errorStatus: true})
    }
  }
  render() {
    return (
      <Container className={"Container"}>
         <Card className={"MainCard"} >
          <Card.Body>
              <Form>
                  <Form.Group className={"SearchBox"} controlId="search">
                    <Form.Text className="text-muted"><h1>Search Giphy in a Jiffy</h1></Form.Text>            
                    <Form.Control type="text" placeholder="Find a Gif" onChange={(event: any) => {
                    
                      this.setState({...this.state, searchTerm: event.currentTarget.value});
                    } } />
                    <Button className={"Button"} onClick={() => {
                      this.setState({...this.state, pageNumber: 0});
                      this.findGifs(0)}}  >Search</Button>
                  </Form.Group>
              </Form>
              { this.renderSearchResults()}
            </Card.Body>
          </Card>
      </Container>
     
    )
  }
  constructor(props: any) {
    super(props);
    this.state = {urls:[], searchTerm: '', pageNumber: 0, groups: [], totalImages: 0, errorStatus: false, searchStatus: false}
  }
  
}


export default App;
